section .bss

arrayNuevo resb 262144

section .text


global aclarar_mm2       
global aclarar_mmx       
global aclarar_assembly   
global promedio_assembly
global multiplyBlend_assembly
global multiplyblend_mmx


aclarar_assembly:
        push    ebp
        mov     ebp, esp

        mov     DWORD  [ebp-8], 0   
for1A:
        cmp     DWORD  [ebp-8], 511 
        jg      salirA
        mov     DWORD  [ebp-12], 0  
for2A:        
        cmp     DWORD  [ebp-12], 511 
        jg      volverA
        ;red
        mov     eax, DWORD  [ebp-8]             
        lea     ecx, [0+eax*4]      
        mov     eax, DWORD  [ebp+8]             
        add     eax, ecx            
        mov     ecx, DWORD  [eax]
        mov     eax, DWORD  [ebp-12]            
        add     eax, ecx
        
        mov     ebx,DWORD[ebp+20]              
        add     DWORD[eax],ebx

        ;green
        mov     eax, DWORD  [ebp-8] 
        lea     ecx, [0+eax*4]      
        mov     eax, DWORD  [ebp+16] 
        add     eax, ecx            
        mov     ecx, DWORD  [eax]
        mov     eax, DWORD  [ebp-12]
        add     eax, ecx
        
        mov     ebx,DWORD[ebp+20]
        add     DWORD[eax],ebx

        ;blue
        mov     eax, DWORD  [ebp-8] 
        lea     ecx, [0+eax*4]      
        mov     eax, DWORD  [ebp+12] 
        add     eax, ecx            
        mov     ecx, DWORD  [eax]
        mov     eax, DWORD  [ebp-12]
        add     eax, ecx

        mov     ebx,DWORD[ebp+20]
        add     DWORD[eax],ebx


               inc     DWORD  [ebp-12]
        jmp     for2A
volverA:
        inc     DWORD  [ebp-8]
        jmp     for1A
salirA:
        nop
        pop     ebp
        ret


multiplyBlend_assembly:
        push    ebp
        push    edi
        push    esi
        push    ebx
        sub     esp, 16
        mov     eax, DWORD  [esp+40]
        mov     ebp, DWORD  [esp+36]
        mov     edi, DWORD  [esp+48]
        mov     esi, DWORD  [esp+52]
        mov     DWORD  [esp+8], eax
        mov     eax, DWORD  [esp+44]
        mov     ebx, DWORD  [esp+56]
        mov     DWORD  [esp+4], eax
        lea     eax, [ebp+2048]
        mov     DWORD  [esp+12], eax
primerFor:
        mov     DWORD  [esp], ebx
        xor     edx, edx
segundoFor:
        mov     ecx, DWORD  [ebp+0]
        mov     ebx, DWORD  [edi]
        add     ecx, edx
        movzx   eax, BYTE  [ecx]
        mul     BYTE  [ebx+edx]
        mov     BYTE  [ecx], al
        mov     eax, DWORD  [esp+8]
        mov     ebx, DWORD  [esi]
        mov     ecx, DWORD  [eax]
        add     ecx, edx
        movzx   eax, BYTE  [ecx]
        mul     BYTE  [ebx+edx]
        mov     ebx, DWORD  [esp]
        mov     BYTE  [ecx], al
        mov     eax, DWORD [esp+4]
        mov     ebx, DWORD  [ebx]
        mov     ecx, DWORD  [eax]
        add     ecx, edx
        movzx   eax, BYTE  [ecx]
        mul     BYTE  [ebx+edx]
        add     edx, 1
        mov     BYTE  [ecx], al
        cmp     edx, 512
        jne     segundoFor
        mov     ebx, DWORD  [esp]
        add     ebp, 4
        add     edi, 4
        add     DWORD  [esp+8], 4
        add     esi, 4
        add     DWORD  [esp+4], 4
        add     ebx, 4
        cmp     DWORD  [esp+12], ebp
        jne     primerFor
        add     esp, 16
        pop     ebx
        pop     esi
        pop     edi
        pop     ebp
        ret

sacarPromedio:
        push    ebp
        mov     ebp, esp
        sub     esp, 32
        mov     DWORD  [ebp-4], 0
        mov     DWORD  [ebp-8], 0
        mov     eax, DWORD  [ebp+8]
        mov     DWORD  [ebp-12], eax
cicloSP1:
        mov     edx, DWORD  [ebp+8]
        mov     eax, DWORD  [ebp+20]
        add     eax, edx
        cmp     DWORD  [ebp-12], eax
        jge     salirSP
        mov     eax, DWORD  [ebp+12]
        mov     DWORD  [ebp-16], eax
cicloSP2:
        mov     edx, DWORD  [ebp+12]
        mov     eax, DWORD  [ebp+20]
        add     eax, edx
        cmp     DWORD  [ebp-16], eax
        jge     volverSP
        mov     eax, DWORD  [ebp-12]
        lea     edx, [0+eax*4]
        mov     edx, DWORD  [ebp+12]
        mov     eax, DWORD  [ebp+20]
        add     eax, edx
        cmp     DWORD  [ebp-16], eax
        jge     volverSP
        mov     eax, DWORD  [ebp-12]
        lea     edx, [0+eax*4]
        mov     eax, DWORD  [ebp+16]
        add     eax, edx
        mov     edx, DWORD  [eax]
        mov     eax, DWORD  [ebp-16]
        add     eax, edx
        mov     al, BYTE  [eax]
        and     eax, 255
        add     DWORD  [ebp-4], eax
        inc     DWORD  [ebp-8]
        inc     DWORD  [ebp-16]
        jmp     cicloSP2
        volverSP:
        inc     DWORD  [ebp-12]
        jmp     cicloSP1
salirSP:
        mov     eax, DWORD  [ebp-4]
        mov     edx, eax
        sar     edx, 31
        idiv    DWORD  [ebp-8]
        mov     DWORD  [ebp-20], eax
        mov     eax, DWORD  [ebp-20]
        leave
        ret
promedio_assembly:
        push    ebp
        mov     ebp, esp
        sub     esp, 16
        mov     DWORD  [ebp-4], 0
cicloP1:
        mov     eax, 512
        sub     eax, DWORD  [ebp+20]
        cmp     DWORD  [ebp-4], eax
        jge     salirP
        mov     DWORD  [ebp-8], 0
cicloP2:
        mov     eax, 512
        sub     eax, DWORD  [ebp+20]
        cmp     DWORD  [ebp-8], eax
         jge     volverP
        push    DWORD  [ebp+20]
        push    DWORD  [ebp+8]
        push    DWORD  [ebp-8]
        push    DWORD  [ebp-4]
        call    sacarPromedio
         add     esp, 16
        mov     edx, eax
        mov     eax, DWORD  [ebp-4]
        lea     ecx, [0+eax*4]
        mov     eax, DWORD  [ebp+8]
        add     eax, ecx
        mov     ecx, DWORD  [eax]
        mov     eax, DWORD  [ebp-8]
        add     eax, ecx
        mov     BYTE  [eax], dl
        push    DWORD  [ebp+20]
        push    DWORD  [ebp+12]
        push    DWORD  [ebp-8]
        push    DWORD  [ebp-4]
        call    sacarPromedio
        add     esp, 16
        mov     edx, eax
        mov     eax, DWORD  [ebp-4]
        lea     ecx, [0+eax*4]
        mov     eax, DWORD  [ebp+12]
        add     eax, ecx
        mov     ecx, DWORD  [eax]
        mov     eax, DWORD  [ebp-8]
        add     eax, ecx
        mov     BYTE  [eax], dl
        push    DWORD  [ebp+20]
        push    DWORD  [ebp+16]
        push    DWORD  [ebp-8]
        push    DWORD  [ebp-4]
        call    sacarPromedio
        add     esp, 16
        mov     edx, eax
         mov     eax, DWORD  [ebp-4]
        lea     ecx, [0+eax*4]
        mov     eax, DWORD  [ebp+16]
        add     eax, ecx
        mov     ecx, DWORD  [eax]
        mov     eax, DWORD  [ebp-8]
        add     eax, ecx
        mov     BYTE  [eax], dl
        inc     DWORD  [ebp-8]
        jmp     cicloP2
volverP:
        inc     DWORD  [ebp-4]
        jmp     cicloP1
salirP:
        nop
        leave
        ret


aclarar_mmx:
    push ebp
    
    mov ebp, esp
    mov ebx, [ebp + 8]
    mov esi, [ebp + 12] 
    mov edi, [ebp + 16]
    mov ecx, 1 
    mov eax, [ebp + 20]
    movd mm4, eax
    jmp ciclo1A

    ciclo1A:
    paddw mm3, mm4  
    cmp ecx, 1
    je ciclo2A
    psllq mm3, 8
    dec ecx
    jmp ciclo1A

    ciclo2A:
    cmp ecx, 65536     
    je salirAMMX
    movd mm0, [ebx+ecx*4]
    movd mm1, [esi+ecx*4]
    movd mm2, [edi+ecx*4]

    paddw mm0, mm3
    paddw mm1, mm3
    paddw mm2, mm3

    movd edx, mm0
    call redA
    movd edx, mm1
    call greenA
    movd edx, mm2
    call blueA
    inc ecx
    jmp ciclo2A
redA:
    cmp edx, 255
    jg volverAMMX
    mov [ebx+ecx*4], edx
    ret
    greenA:
    cmp edx, 255
    jg volverAMMX
    mov [esi+ecx*4], edx
    ret
blueA:
    cmp edx, 255
    jg volverAMMX
    mov [edi+ecx*4], edx
    ret

salirAMMX:
    EMMS
    mov esp, ebp
    pop ebp
    ret

volverAMMX:
    ret



multiplyblend_mmx:
   push ebp
   mov ebp,esp

   mov edx,0  ; INDICE CANTIDAD DE PIXELES
    
   cicloMultiplicacion:
   mov eax, [ebp+8]  ; ROJO1
   mov ebx, [ebp+20] ; ROJO2

   mov esi, [ebp+12] ; VERDE1
   mov edi, [ebp+24] ;VERDE2

   cmp edx, 65536    ; TOPE DE PIXELES
   je salir

   movd mm0, [eax+edx*4]    ;rojos1
   movd mm1, [ebx+edx*4]    ; rojos2 

   pxor mm6,mm6
   movq mm7,mm0      ; copia del rojo1
   movq mm5,mm1      ; copia del rojo2
   
   PUNPCKLBW mm0,mm6  ; unpack low de rojo1
   PUNPCKHBW mm7,mm6  ;unpack high de la copia del rojo1

   PUNPCKLBW mm1,mm6  ; unpack low de rojo2
   PUNPCKHBW mm5,mm6  ; unpack high de rojo2

   PMULLW  mm0,mm1    ; multiplicamos el low
   PMULLW  mm7,mm5    ; multiplicamos el high
  
   PACKUSWB mm0,mm7  ; hacemos el pack

   inc edx
   movd ecx, mm0
   call pasarRojos

   pxor mm0,mm0
   pxor mm1,mm1
   pxor mm5,mm5
   pxor mm6,mm6
   pxor mm7,mm7


   movd mm0, [edi+edx*4]  ; TOMAMOS LOS VERDES
   movd mm1, [esi+edx*4]  ; TOMAMOS LOS VERDES

   pxor mm6,mm6
   movq mm7,mm0      ; copia del VERDE1
   movq mm5,mm1      ; copia del VERDE2
   
   PUNPCKLBW mm0,mm6  ; unpack low de VERDE1
   PUNPCKHBW mm7,mm6  ;unpack high de la copia del VERDE1

   PUNPCKLBW mm1,mm6  ; unpack low de VERDE2
   PUNPCKHBW mm5,mm6  ; unpack high de VERDE2

   PMULLW  mm0,mm1    ; multiplicamos el low
   PMULLW  mm7,mm5    ; multiplicamos el high
  
   PACKUSWB mm0,mm7  ; hacemos el pack

   
   movd ecx, mm0
   call pasarVerdes

   pxor mm0,mm0
   pxor mm1,mm1
   pxor mm5,mm5
   pxor mm6,mm6
   pxor mm7,mm7



   mov  eax, [ebp+16]     ; AZULES1 
   mov  ebx, [ebp+28]      ; AZULES 2

   movd mm1, [eax+edx*4]   ;pasamos los azules a mm1
   movd mm0, [ebx+edx*4]    ;pasmos los azules a mm2


   pxor mm6,mm6
   movq mm7,mm0      ; copia del azul1
   movq mm5,mm1      ; copia del azul2

   PUNPCKLBW mm0,mm6  ; unpack low de azul1
   PUNPCKHBW mm7,mm6  ;unpack high de la copia del azul2

   PUNPCKLBW mm1,mm6  ; unpack low de azul1
   PUNPCKHBW mm5,mm6  ; unpack high de azul2

   PMULLW  mm0,mm1    ; multiplicamos el low
   PMULLW  mm7,mm5    ; multiplicamos el high

   PACKUSWB mm0,mm7  ; hacemos el pack

   movd ecx, mm0
   call pasarAzules

   jmp cicloMultiplicacion
   
   salir:
   EMMS
   mov esp,ebp
   pop ebp
   ret


  pasarRojos:
  cmp ecx, 255
  jg cicloMultiplicacion
  mov [eax+edx*4], ecx
  ret


  pasarVerdes:
  cmp ecx, 255
  jg cicloMultiplicacion
  mov [esi+edx*4], ecx
  ret

  pasarAzules:
  cmp ecx, 255
  jg cicloMultiplicacion
  mov [eax+edx*4], ecx
  ret

  

    